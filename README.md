# Specyfikacja

Urządzenie jest interfejsem (mostkiem) USB 2.0 <-> UART.

- Układ FTDI FT231X (https://ftdichip.com/products/ft231xs)
- Gniazdo USB typu B micro
- Poziomy napięciowe UART wybierane zworkami 2,0 mm: +2,8 V lub +3,3 V
- Sygnały wejściowe tolerują +5,8 V
- Wyprowadzone styki UART:
  - 1 = TXD (out)
  - 2 = RXD (in)
  - 3 = GND
  - 4 = RTS (out)
  - 5 = CTS (in)
  - 6 = DTR (out)
  - 7 = DSR (in)
  - 8 = DCD (in)
  - 9 = RI (in)
- Złącze UART: angled header 1x9 female raster=2,54 mm
- LED TX czerwona
- LED RX zielona
- Rozmiary PCB: 23,4 x 39,0 mm (szerokość x długość)

# Projekt PCB

Schemat: [doc/FT231_UART_V1_0_SCH.pdf](doc/FT231_UART_V1_0_SCH.pdf)

Widok 3D: [doc/FT231_UART_V1_0_3D.pdf](doc/FT231_UART_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

![](resource/pcb_3d.png "PCB 3D")

![](resource/pcb.png "PCB top")

# Licencja

MIT
